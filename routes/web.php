<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::resource('todo', 'TodosController');//susikuria visi route
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//susikuriam Route naujai funkcijai aprasytai controller
Route::post('/todo/change-status/{id}/{status}', 'todosController@changeStatus')->name('todo.changestatus');
