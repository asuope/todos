@extends("layouts.app")

@section('content')


 <div class="container">
          {{-- klaidu isvedimas pagal Laravel validatoriu --}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('todo.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            
            <input
                    class="form-control"
                    placeholder="uzduotis"
                    value="{{ old('uzduotis') }}"
                    type="text" 
                    name="todo">

            <input
                    class="form-control" 
                    placeholder="busena"
                    value="{{ old('busena') }}"
                    type="text" 
                    name="busena">
            
            
            <input class="btn btn-success" type="submit" value="ivesti">
        </form>
        
    </div>
    <table class="table">
           <h3>Uzduociu sarasas</h3>
           <thead>
                <tr>
                    <th>Uzduotis </th>
                    <th>Busena</th>
                    <th>Veiksmas</th>
                    <th>Veiksmas</th>
                                     
                </tr>
            </thead>
            @foreach ($todo as $todo)
                <tr>
                    <td>{{ $todo->todo }} </td>
                    <td>{{ $todo->busena }} </td>
                    <td>
                        @if($todo->busena == 1)
					<button class="btn info">Uzduotis atlikta</button>
					<form action="{{ route('todo.changestatus', [$todo->id, 0])}}" method="POST">
						{{ csrf_field() }}
						<input type="submit" value="Padaryti kaip nebaigta" class="btn btn-success">
					</form>
				@else
					<form action="{{ route('todo.changestatus', [$todo->id, 1])}}" method="POST">
						{{ csrf_field() }}
						<input type="submit" value="Baigti" class="btn btn-success">
					</form>
				@endif
                    </td>
                     <td>
                        <form action="{{ route('todo.destroy', $todo
                        ->id) }}" method="POST">
                            <!--  Su post metodu dirbant sau formom visada butina ideti sita laukeli  -->
                        {{ csrf_field() }}
                        {{ method_field('delete')}}
                            <input 
                            class="btn btn-danger"
                            type="submit" value="Ištrinti">
                        </form>
                    </td>


                </tr>
            @endforeach
    </table>                
@endsection