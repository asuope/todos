<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;  
use Session; 
use App\Todo;
use App;
use Gate;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todo = Todo::all();
        return view( 'todo.index',["todo" =>$todo] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'todo.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
    id - todo list item id
    status - status of item
        -- 0 = undone
        -- 1 = done
        -- 2 = waiting for review 
        susikuriame funkcija nauja kuri mums keis busena
    */
    public function changeStatus($id, $busena) {
        $todo = Todo::find($id);

        $todo->busena = $busena;

        $todo->save();

        return redirect()->route('todo.index');
    }

    public function store(Request $request)
    {
        $todo                       = new Todo;
        $todo->todo  = $request->todo;
        $todo->busena = $request->busena;
        $todo->save();
       Session::flash( 'status', 'Uzduotis pateikta' );


       return redirect()->route( 'todo.index' );//
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $todo = Todo::find($id);
        $todo->delete();
        Session::flash( 'status', 'Info ištrinta sėkmingai !' );
        return redirect()->route('todo.index');//istrinam ir einam i puslapi su visais automobiliais
    }
}
